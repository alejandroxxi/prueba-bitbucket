from dht22 import dht22
from bh1750 import bh1750
#from lmv324m import lmv324m
#from lmvsocket import lmv324m_test

class Product:
    def __init__(self):
        self.configurations = { 
           0: { 0: dht22.readTemperature, 1: dht22.readHumidity, 2: bh1750.readLight, 3: "Socket" },
        }
    
    def setConfiguration(self, type):
        return self.configurations[type]

product = Product()