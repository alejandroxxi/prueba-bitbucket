import RPi.GPIO as GPIO
import time

class Leds:
    def __init__(self, leds):
        self.leds = leds

        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)

        for led in self.leds.values():
            GPIO.setup(led['pin'], GPIO.OUT, initial=led['initial'])

    def turnOn(self, led):
        pin = led['pin']
        if not GPIO.input(pin): GPIO.output(pin, 1)

    def turnOff(self, led):
        pin = led['pin']
        if GPIO.input(pin): GPIO.output(pin, 0)

    def blink(self, led, seconds = 0.05, loops = 10):
        i = 0
        self.oneLight(led)
        
        while i < loops:
            self.turnOff(led)
            time.sleep(seconds)
            self.turnOn(led)
            time.sleep(seconds)

            i += 1

    def oneLight(self, led):
        for key, value in self.leds.items():
            self.turnOn(led) if led['pin'] == value['pin'] else self.turnOff(self.leds[key])
            
EXPERTA_MIL = {
    'success': {'pin': 36, 'initial': 1},
    'warning': {'pin': 37, 'initial': 1},
    'error': {'pin': 32, 'initial': 1}
}

LEDS = Leds(EXPERTA_MIL)

success = LEDS.leds['success']
warning = LEDS.leds['warning']
error = LEDS.leds['error']