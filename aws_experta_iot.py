from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import time, argparse, json

class AWSExpertaIot:
    def __init__(self, credentials):
        self.host = "a14ifzhh6b83pg.iot.us-east-1.amazonaws.com"
        self.rootCAPath = "VeriSign-Class 3-Public-Primary-Certification-Authority-G5.pem"
        self.certificatePath = "cf055fd78b-certificate.pem.crt"
        self.privateKeyPath = "cf055fd78b-private.pem.key"
        self.serial = credentials.serial
        self.topic = "iot/data"
        self.AWSExpertaIotClient = None
        
        self._startExpertaIot()
        
    def _initializeMQTTClient(self):
        self.AWSExpertaIotClient = AWSIoTMQTTClient(self.serial)
        self.AWSExpertaIotClient.configureEndpoint(self.host, 8883)
        self.AWSExpertaIotClient.configureCredentials(self.rootCAPath, self.privateKeyPath, self.certificatePath)
        
    def _configureMQTTClient(self):
        self.AWSExpertaIotClient.configureAutoReconnectBackoffTime(1, 32, 20)
        self.AWSExpertaIotClient.configureOfflinePublishQueueing(-1)
        self.AWSExpertaIotClient.configureDrainingFrequency(2)
        self.AWSExpertaIotClient.configureConnectDisconnectTimeout(30)
        self.AWSExpertaIotClient.configureMQTTOperationTimeout(30)
    
    def connectMQTTClient(self):
        self.AWSExpertaIotClient.connect()
        time.sleep(2)
        
    def _startExpertaIot(self):
        self._initializeMQTTClient()
        self._configureMQTTClient()
        self.connectMQTTClient()