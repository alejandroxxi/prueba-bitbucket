from credentials import credentials, Credentials
from aws_experta_iot import AWSExpertaIot
from product import product
from logger import logger_experta, log
from led import LEDS
import requests, json, time, ast, bash_font, sys, logging, socket, sys
import RPi.GPIO as GPIO

def recordActivity():
    with open('last_date.txt', 'w') as file:
        file.write(credentials.getDate("%Y%m%d%H%M%S"))
        file.close()
    log.info("Registering new activity date in file last_date.txt")

def checkLastActivity():
    try:
        with open('last_date.txt', 'rt') as file:
            date = file.read()
            file.close()
        log.info("The last_date.txt file was read and an activity date was found: {}".format(date))
        return date
    except:
        log.info("The file last_date.txt does not exist. One will be created instead")
        recordActivity()

def createPayload(status, publish):
    global sensors, experta_iot
    
    read_time = checkLastActivity()

    #if logger_experta.checkFileSize():
        # log = logger_experta.setup('experta_logger', 'experta_logger.log')

    for idx, sensor in enumerate(sensors):
        LEDS.oneLight(success)
        payload = {
            "nroSensor": sensor["nroSensor"],
            "serial": ("{}-{}".format(credentials.serial, str(sensor["nroSensor"]))),
            "readTime": read_time,
            "metric": sensor["metric"],
            "value": 0,
            "lowest": sensor["lowest"],
            "low": sensor["low"],
            "high": sensor["high"],
            "highest": sensor["highest"],
            "delta": sensor["delta"],
            "status": status
        }

        if status == "normal" and previous_status[idx] != "outofservice":
            payload["value"] = PRODUCT[idx]()
            current_time = credentials.getDate("%H:%M")

            if payload["value"]:
                if publish or previous_status[idx] == "onservice":
                    publishPayload(payload)
                    continue

                difference_readings = abs(payload["value"] - previous_reading[idx])
                log.info("Sensor ({}): Current {}; Previous {}; Metric {};".format(payload["nroSensor"], payload["value"], previous_reading[idx], payload["metric"]))

                if difference_readings >= payload["delta"] and previous_reading[idx]:
                    log.warning("Sensor {} has reported a reading higher than the delta {}".format(payload["nroSensor"], payload["delta"]))
                    publishPayload(payload)
                
                if current_time in ["23:59", "00:00"]:
                    log.info("Sensor {} reading sent at scheduled time".format(payload["nroSensor"]))
                    if not idx:
                        try:
                            experta_iot = AWSExpertaIot(Credentials())
                            sensors = getConfiguration(Credentials())["sensors"]
                        except:
                            continue
                    publishPayload(payload)
                    
                print("Sensor: {}, Old Value: {}, New Value: {}, Delta: {}".format(payload["nroSensor"], previous_reading[idx], payload["value"], payload["delta"]))
                previous_reading[idx] = payload["value"]
            
            else:
                LEDS.blink(error, 0.05, 20)
                log.error("Error getting sensor {} data. Check the connection of the wires".format(payload["nroSensor"]))
                
                if previous_status[idx] != "outofservice":
                    payload["status"] = "outofservice"
                    publishPayload(payload)
        else:
            if status == "onservice" and not PRODUCT[idx]():
                previous_status[idx] = "outofservice"
                continue
            
            if previous_status[idx] == "outofservice" and PRODUCT[idx]():
                payload["status"] = "onservice"
            
            elif previous_status[idx] == "outofservice" and not PRODUCT[idx]():
                LEDS.blink(error, 0.05, 20)
                log.error("Error getting sensor {} data. Check the connection of the wires".format(payload["nroSensor"]))
                continue
            
            publishPayload(payload)

        time.sleep(2)

    recordActivity()
    LEDS.blink(success)

def publishPayload(payload):
    logger = logging.getLogger("AWSIoTPythonSDK.core")
    logger.setLevel(logging.DEBUG)
    streamHandler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    streamHandler.setFormatter(formatter)
    logger.addHandler(streamHandler)

    try:
        topic = experta_iot.topic
        experta_iot.AWSExpertaIotClient.publish(topic, json.dumps(payload), 1)
        LEDS.blink(warning)
        log.info("Published sensor {} on topic {} ({}, {})".format(payload["nroSensor"], topic, payload["status"], payload["value"]))
        previous_status[payload["nroSensor"] - 1] = payload["status"]
    except:
        experta_iot.connectMQTTClient()
        LEDS.blink(error)
        log.error("MQTT Publish failed - Connect time out")

def getConfiguration(credentials):
    log.info("Starting new HTTP connection: iot.expertaart.com.ar")
    link = ("{}/{}/{}".format(credentials.url, credentials.serial, credentials.token))
    
    try:
        response = requests.get(link)
        if response.status_code == 200:
            log.info("200: {}".format(link))
            configuration = ast.literal_eval(response.content)
            LEDS.blink(success)
            log.info("Configuration for {} sensors has been found".format(len(configuration["sensors"])))
            return configuration
        else:
            raise RuntimeError
    except:
        LEDS.oneLight(error)
        log.error("404: Failed starting new HTTP connection")
        print("{}Failed starting new HTTP connection (404)".format(bash_font.red))
        return getConfiguration(Credentials())

def readSocket():
    print("Connection from: ", addr_socketc)
    sound = 1
    
    try:
        data_socket = connection.recv(1024)
        if data_socket:
            sound_readings = sorted(list(map(int, set(data_socket.decode("utf-8").split(",")[1:-1]))))
            print(sound_readings)
            sound = sound_readings[-1]
    except:
        sound = 1

    return sound

success = LEDS.leds['success']
warning = LEDS.leds['warning']
error = LEDS.leds['error']

time.sleep(2)

LEDS.oneLight(success)

experta_iot = AWSExpertaIot(Credentials())
configuration = getConfiguration(Credentials())

sound_socket = socket.socket()
sound_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sound_socket.bind(("localhost", 9998))
sound_socket.listen(100)

connection, addr_socketc = sound_socket.accept()
connection.settimeout(5)

PRODUCT = product.setConfiguration(0)
previous_reading = [False] * len(PRODUCT)
previous_status = [False] * len(PRODUCT)

PRODUCT[3] = readSocket

sensors = configuration["sensors"]

createPayload("outofservice", True)
createPayload("onservice", True)
createPayload("normal", True)

while True:
    time.sleep(5)
    try:
        createPayload("normal", False)
    except:
        continue