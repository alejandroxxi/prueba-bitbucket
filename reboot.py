import os, datetime, time
from logger import Logger

previous_count = 0
logger_reboot = Logger()

log = logger_reboot.setup('logger_reboot', 'reboot.log')

while True:
    log.info("Running script reboot")
    count = 0

    with open('/home/pi/experta_logger.log', 'rt') as file:
        for line in file.xreadlines():
            count+= 1
        file.close()
    log.info("Previous count: {}".format(previous_count))
    log.info("Current count: {}".format(count))
    if previous_count == count and previous_count:
        log.warning("Reboot automatized")
        os.system("sudo shutdown -r now")
        break
    previous_count = count
    time.sleep(300)