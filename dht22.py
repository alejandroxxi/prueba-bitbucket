import Adafruit_DHT, logging

class DHT22:
    def __init__(self):
        self.sensor = Adafruit_DHT.DHT22
        self.pin = 27
        
    def readHumidity(self):
        try:
            humidity = int(Adafruit_DHT.read_retry(self.sensor, self.pin)[0])
            if humidity > 1000: raise TypeError
        except TypeError:
            humidity = 0
        return humidity
        

    def readTemperature(self):
        try:
            temperature = int(Adafruit_DHT.read_retry(self.sensor, self.pin)[1])
        except TypeError:
            temperature = 0
        return temperature

dht22 = DHT22()