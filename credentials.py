import hashlib
import datetime

class Credentials:
    def __init__(self):
        self._getCredentials()
        self.url = "http://iot.expertaart.com.ar:8080/iot/raspy/getConfiguration"
        
    def _getSerial(self):
        file = open('/proc/cpuinfo', 'r')
        for line in file:
            if line[0:6] == 'Serial': serial = line[10:26]
        file.close()
        return serial
    
    def getDate(self, format):
        date = datetime.datetime.now().strftime(format)
        return date
    
    def _toBinary(self, string):
        binary = bytes(string)
        return binary
    
    def _generateToken(self, date, serial):
        token = hashlib.sha256(date + serial).hexdigest()
        return token
    
    def _getCredentials(self):
        self.serial = self._getSerial()
        self.date = self.getDate("%d-%m-%Y")
        self.token = self._generateToken(self._toBinary(self.date + "-"), self._toBinary(self.serial))

credentials = Credentials()