import smbus, time

class BH1750:
    def __init__(self):
        self.device = 0x23
        self.power_down = 0x00
        self.power_on = 0x01
        self.reset = 0x07

        self.continuos_low_res_mode = 0x13
        self.continuos_high_res_mode_1 = 0x10
        self.continuos_high_res_mode_2 = 0x11
        self.one_time_high_res_mode_1 = 0x20
        self.one_time_high_res_mode_2 = 0x21
        self.one_time_low_res_mode = 0x23

        self.bus = smbus.SMBus(1)

    def _convertToNumber(self, data):
        return ((data[1] + (256 * data[0])) / 1.2)

    def readLight(self):
        try:
            data = self.bus.read_i2c_block_data(self.device, self.one_time_high_res_mode_1)
            light = int(self._convertToNumber(data))
            if light == 0: light = 1
            return light
        except:
            return 0

bh1750 = BH1750()