import socket, serial, time, os

class LMV324M:
    def __init__(self):
        self.arduino = None
        self.sound = None

    def openSerial(self):
        try:
            self.arduino = serial.Serial('/dev/ttyACM0', 9600, timeout = 1)
            self.arduino.flushInput()
            return self.arduino
        except:
            try:
                self.arduino = serial.Serial('/dev/ttyACM1', 9600, timeout = 1)
                self.arduino.flushInput()
                return self.arduino
            except:
                return 0
    def readSound(self, arduino): 
        while True:
            self.sound = arduino.readline()
            try:
                decoded_bytes = float(self.sound[0:len(self.sound)-2].decode("utf-8"))
                if int(decoded_bytes):
                    self.sound = int(decoded_bytes)
                    break
            except:
                continue
        return self.sound

    def sendSocket(self):
        sound_socket = socket.socket()
        sound_socket.connect(("localhost", 9998))
        arduino = self.openSerial()
        low = False
        while True:
            try:
                sound_reading = self.readSound(arduino)
                data_socket = "{},".format(sound_reading)
                if not low and sound_reading == 1:
                    sound_socket.send(str(data_socket).encode())
                    print("Sending socket data: {}".format(data_socket))
                    low = True
                elif sound_reading > 1:
                    sound_socket.send(str(data_socket).encode())
                    print("Sending socket data: {}".format(data_socket))
                    low = False
            except:
                sound_socket.send(str("1").encode())

lmv324m_test = LMV324M()

while True:
    try:
        lmv324m_test.sendSocket()
    except:
        continue