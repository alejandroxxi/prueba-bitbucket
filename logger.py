import logging, os

class Logger:
    def __init__(self):
        self.formatter = logging.Formatter("%(asctime)s [%(levelname)s] %(message)s")

    def setup(self, name, log_file, level=logging.INFO):
        handler = logging.FileHandler(log_file)        
        handler.setFormatter(self.formatter)

        setup = logging.getLogger(name)
        setup.setLevel(level)
        setup.addHandler(handler)

        return setup

    def checkFileSize(self, path = "/home/pi/experta_logger.log"):
        size = int(os.path.getsize(path))
        max_size = 8589934592
        name = path.split("/")[-1]
        if size > max_size:
            os.remove(path)
            file = open(name,"w+")
            file.close()
            return True
        return False

logger_experta = Logger()
log = logger_experta.setup('experta_logger', 'experta_logger.log')